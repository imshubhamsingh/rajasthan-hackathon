import { database } from "../firebase";
export const ADD_SEVERITY = "add_severity";
export const ADD_TIME = "add_time";
export const ADD_PLACE = "add_place";
export const ADD_INJURY = "add_injury";
export const ADD_TYPE = "add_type";

export const accidentInfoCollect = (type, value) => async dispatch => {
  dispatch({
    type: type,
    payload: value
  });
};

export const sendToDatabase = accidentInfo => {
  return dispatch => {
    database.ref("accidents").push(accidentInfo);
  };
};
