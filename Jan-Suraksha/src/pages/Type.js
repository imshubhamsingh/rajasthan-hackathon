import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Header from "../components/Header";
import Stepper from "../components/Stepper";
import Footer from "../components/Footer";
import IncidentType from "../components/IncidentType";

import CircularProgress from "material-ui/CircularProgress";
import { ADD_TYPE, accidentInfoCollect } from "../actions/accidentInfoAction";

class Type extends Component {
  state = {
    showLoader: false
  };
  sendValue = value => {
    this.setState({
      showLoader: true
    });
    setTimeout(() => {
      this.props.accidentInfoCollect(ADD_TYPE, { type: value });
      this.props.history.push("/victim");
    }, 1500);
  };
  render() {
    return (
      <Fragment>
        <Stepper step={2} />
        <Header
          title="What happened?"
          style={{
            fontSize: "32px",
            padding: "24px 10px"
          }}
        />
        <div>
          <IncidentType sendValue={this.sendValue} />
        </div>
        <div style={{ width: "100%" }}>
          {this.state.showLoader ? (
            <CircularProgress
              size={60}
              thickness={7}
              left={-10}
              top={10}
              status={"loading"}
              style={{ marginLeft: "41%", marginTop: "82px", fill: "#ff7d16" }}
            />
          ) : (
            ""
          )}
        </div>
        <Footer back="/severity" />
      </Fragment>
    );
  }
}

export default connect(null, { accidentInfoCollect })(Type);
