import React, { Component, Fragment } from "react";
import Header from "../components/Header";
import Swipe from "../components/Swipe";

class Home extends Component {
  goToNext = () => {
    this.props.history.push("/severity");
  };
  render() {
    return (
      <Fragment>
        <Header title="Incident" step={1} />
        <div style={{ marginTop: "22vh" }}>
          <Swipe goToNext={this.goToNext} />
        </div>
      </Fragment>
    );
  }
}

export default Home;
