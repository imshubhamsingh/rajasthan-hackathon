import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Header from "../components/Header";
import Stepper from "../components/Stepper";
import Footer from "../components/Footer";

import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";
import CircularProgress from "material-ui/CircularProgress";

import HumanBody from "../components/HumanBody";

import { ADD_INJURY, accidentInfoCollect } from "../actions/accidentInfoAction";

class Victim extends Component {
  state = {
    injured: false,
    open: false,
    injuredPlace: "",
    showloader: false
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleSubmit = () => {
    this.setState({ open: false });
    this.setState({
      showLoader: true
    });
    setTimeout(() => {
      this.props.accidentInfoCollect(ADD_INJURY, {
        injury: this.state.injuredPlace
      });

      this.props.history.push("/time");
    }, 1000);
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  getValue = value => {
    this.setState({
      injuredPlace: value
    });
    this.handleOpen();
  };

  render() {
    const actions = [
      <FlatButton label="Cancel" primary={true} onClick={this.handleClose} />,
      <FlatButton label="Submit" primary={true} onClick={this.handleSubmit} />
    ];
    return (
      <Fragment>
        <Stepper step={3} />
        <Header
          title="Did anyone sustain any injury?"
          style={{
            fontSize: "32px",
            padding: "24px 10px"
          }}
        />
        {this.state.injured ? (
          <div style={{ width: "267px", margin: "-57px auto" }}>
            <HumanBody getValue={this.getValue} />
          </div>
        ) : (
          <div style={{ display: "flex", justifyContent: "space-around" }}>
            <RaisedButton
              label="Yes"
              onClick={() =>
                this.setState({
                  injured: true
                })
              }
            />
            <RaisedButton label="No" onClick={() => this.handleSubmit()} />
          </div>
        )}
        <Dialog title="" actions={actions} modal={true} open={this.state.open}>
          You sure about this?
        </Dialog>
        <div style={{ width: "100%" }}>
          {this.state.showLoader ? (
            <CircularProgress
              size={60}
              thickness={7}
              left={-10}
              top={10}
              status={"loading"}
              style={{ marginLeft: "41%", marginTop: "82px", fill: "#ff7d16" }}
            />
          ) : (
            ""
          )}
        </div>
        <Footer back="/incidentType" />
      </Fragment>
    );
  }
}

export default connect(null, { accidentInfoCollect })(Victim);
