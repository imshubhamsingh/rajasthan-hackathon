import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Header from "../components/Header";
import Stepper from "../components/Stepper";
import Footer from "../components/Footer";
import SeverityBox from "../components/SeverityBox";

import CircularProgress from "material-ui/CircularProgress";
import {
  ADD_SEVERITY,
  accidentInfoCollect
} from "../actions/accidentInfoAction";

class Severity extends Component {
  state = {
    showLoader: false
  };
  sendValue = value => {
    this.setState({
      showLoader: true
    });
    setTimeout(() => {
      this.props.accidentInfoCollect(ADD_SEVERITY, { severity: value });
      this.props.history.push("/incidentType");
    }, 1500);
  };
  render() {
    return (
      <Fragment>
        <Stepper step={1} />
        <Header
          title="Severity?"
          style={{
            fontSize: "32px",
            padding: "24px 10px"
          }}
        />
        <div>
          <SeverityBox sendValue={this.sendValue} />
        </div>
        <div style={{ width: "100%" }}>
          {this.state.showLoader ? (
            <CircularProgress
              size={60}
              thickness={7}
              left={-10}
              top={10}
              status={"loading"}
              style={{ marginLeft: "41%", marginTop: "82px", fill: "#ff7d16" }}
            />
          ) : (
            ""
          )}
        </div>
        <Footer back="/" />
      </Fragment>
    );
  }
}

export default connect(null, { accidentInfoCollect })(Severity);
