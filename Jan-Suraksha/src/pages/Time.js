import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Header from "../components/Header";
import Stepper from "../components/Stepper";
import Footer from "../components/Footer";

import TimePicker from "material-ui/TimePicker";
import DatePicker from "material-ui/DatePicker";
import CircularProgress from "material-ui/CircularProgress";
import { ADD_TIME, accidentInfoCollect } from "../actions/accidentInfoAction";

class Time extends Component {
  state = {
    date: null,
    time: null,
    showLoader: false
  };

  checkLoader = () => {
    if (this.state.date !== null && this.state.time !== null) {
      this.setState({
        showLoader: true
      });
      setTimeout(() => {
        this.props.accidentInfoCollect(ADD_TIME, {
          date: this.state.date,
          time: this.state.time
        });
        this.props.history.push("/place");
      }, 1000);
    }
  };
  dateSetOrChanged = async (first, newDate) => {
    var newd = new Date(newDate);
    await this.setState({
      date: `${newd.getDay()}/${newd.getMonth()}/${newd.getFullYear()}`
    });
    this.checkLoader();
  };

  timeSetOrChanged = async (first, newTime) => {
    var newd = new Date(newTime);
    await this.setState({ time: `${newd.getHours()}:${newd.getMinutes()}` });
    this.checkLoader();
  };

  render() {
    return (
      <Fragment>
        <Stepper step={4} />
        <Header
          title="When was it?"
          style={{
            fontSize: "32px",
            padding: "24px 10px"
          }}
        />
        <div style={{ width: "267px", margin: "25vh auto" }}>
          <TimePicker
            hintText="Select Time for Accident"
            autoOk={true}
            onChange={this.timeSetOrChanged}
          />
          <DatePicker
            hintText="Select Date for Accident"
            onChange={this.dateSetOrChanged}
          />
        </div>

        {this.state.showLoader ? (
          <CircularProgress
            size={60}
            thickness={7}
            left={-10}
            top={10}
            status={"loading"}
            style={{
              marginLeft: "41%",
              marginTop: "-82px",
              fill: "#ff7d16",
              width: "100%"
            }}
          />
        ) : (
          ""
        )}

        <Footer back="/victim" />
      </Fragment>
    );
  }
}

export default connect(null, { accidentInfoCollect })(Time);
