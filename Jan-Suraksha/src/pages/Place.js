import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import Header from "../components/Header";
import Stepper from "../components/Stepper";
import Footer from "../components/Footer";

import RaisedButton from "material-ui/RaisedButton";

import Map from "../components/Map";

import {
  accidentInfoCollect,
  sendToDatabase,
  ADD_PLACE
} from "../actions/accidentInfoAction";

class Place extends Component {
  componentDidMount() {
    this.props.accidentInfoCollect(ADD_PLACE, {
      lat: 53.32,
      lng: -7.77
    });
  }
  render() {
    return (
      <Fragment>
        <Stepper step={5} />
        <Header
          title="Where it happened?"
          style={{
            fontSize: "32px",
            padding: "24px 10px"
          }}
        />
        <div>
          <Map />
          <RaisedButton
            label="Other Location"
            fullWidth={true}
            style={{ marginTop: "-40px" }}
          />
        </div>

        <RaisedButton
          label="Full width"
          fullWidth={true}
          label="Submit"
          onClick={() => {
            console.log(this.props.accidentInfo);
            this.props.sendToDatabase(this.props.accidentInfo);
          }}
        />

        <Footer back="/time" />
      </Fragment>
    );
  }
}

const mapStatesToProps = state => {
  return {
    accidentInfo: state.accidentInfo
  };
};

export default connect(mapStatesToProps, {
  accidentInfoCollect,
  sendToDatabase
})(Place);
