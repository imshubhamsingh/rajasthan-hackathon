import {
  ADD_INJURY,
  ADD_PLACE,
  ADD_SEVERITY,
  ADD_TIME,
  ADD_TYPE
} from "../actions/accidentInfoAction";

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_INJURY:
      return { ...state, ...action.payload };
    case ADD_PLACE:
      return { ...state, ...action.payload };
    case ADD_TIME:
      return { ...state, ...action.payload };
    case ADD_TYPE:
      return { ...state, ...action.payload };
    case ADD_SEVERITY:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
