import { combineReducers } from "redux";
import accidentInfoReducer from "./accidentInfoReducer";

export default combineReducers({
  accidentInfo: accidentInfoReducer
});
