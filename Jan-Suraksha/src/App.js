import React, { Component } from "react";
import { Switch, Route } from "react-router";

import Home from "./pages/Home";
import Severity from "./pages/Severity.js";
import Time from "./pages/Time";
import Place from "./pages/Place";
import Victim from "./pages/Victim";
import Type from "./pages/Type";

import "./App.scss";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/severity" component={Severity} />
        <Route path="/victim" component={Victim} />
        <Route path="/time" component={Time} />
        <Route path="/place" component={Place} />
        <Route path="/incidentType" component={Type} />
      </Switch>
    );
  }
}

export default App;
