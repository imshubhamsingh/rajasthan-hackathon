import React, { Component } from "react";

import "./incidenttype.scss";

class IncidentType extends Component {
  state = {
    high: false,
    medium: false,
    slight: false,
    normal: false
  };

  select = value => {
    this.setState({
      fire: false,
      theft: false,
      accident: false,
      property_damage: false
    });
    if (value === "fire") {
      this.setState({ fire: true });
    } else if (value === "theft") {
      this.setState({ theft: true });
    } else if (value === "accident") {
      this.setState({ accident: true });
    } else {
      this.setState({ property_damage: true });
    }
    this.props.sendValue(value);
  };

  render() {
    return (
      <div className="box">
        <div
          className={`option " + ${this.state.fire ? "selected" : ""}`}
          onClick={() => {
            this.select("fire");
          }}
        >
          Fire
        </div>
        <div
          className={`option " + ${this.state.theft ? "selected" : ""}`}
          onClick={() => {
            this.select("theft");
          }}
        >
          Theft
        </div>
        <div
          className={`option " + ${this.state.accident ? "selected" : ""}`}
          onClick={() => {
            this.select("accident");
          }}
        >
          Accident
        </div>
        <div
          className={`option " + ${
            this.state.property_damage ? "selected" : ""
          }`}
          onClick={() => {
            this.select("property_damage");
          }}
        >
          Property Damage
        </div>
      </div>
    );
  }
}

export default IncidentType;
