import React, { Component } from "react";
import "./header.scss";

class Header extends Component {
  render() {
    return (
      <div className="header" style={this.props.style ? this.props.style : {}}>
        {this.props.title}
      </div>
    );
  }
}

export default Header;
