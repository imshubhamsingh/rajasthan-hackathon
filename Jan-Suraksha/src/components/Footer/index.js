import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./footer.scss";

class Footer extends Component {
  render() {
    return (
      <Link to={this.props.back} className="footer">
        <i className="fas fa-long-arrow-alt-left" /> Back
      </Link>
    );
  }
}

export default Footer;
