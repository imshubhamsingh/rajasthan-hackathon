import React, { Component } from "react";
import Hammer from "react-hammerjs";

import "./swipe.scss";

class Swipe extends Component {
  state = {
    distance: 0
  };
  sendSeverity = event => {
    console.log(event.distance);

    this.setState({
      distance: event.distance
    });

    if (this.state.distance > 294) {
      this.props.goToNext();
    }
  };
  render() {
    return (
      <Hammer onSwipe={this.sendSeverity}>
        <div className="slider">
          <div className="text">Swipe right to report an incident</div>

          <div
            className="slider-circle"
            style={{
              transform: `translateX(${Math.min(this.state.distance, 294)}px)`
            }}
          >
            <i className="fas fa-hand-point-right" />
          </div>
        </div>
      </Hammer>
    );
  }
}

export default Swipe;
