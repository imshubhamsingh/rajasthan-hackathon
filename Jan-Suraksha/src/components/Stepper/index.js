import React, { Component } from "react";

import "./stepper.scss";

class Stepper extends Component {
  render() {
    return (
      <div className="stepper">
        <div className="first-step">
          <i className="fas fa-play" />
        </div>
        <div
          className="step"
          style={{
            backgroundColor: `${this.props.step > 1 ? "#ff4d74" : ""}`
          }}
        >
          <i
            className="fas fa-play"
            style={{
              color: `${this.props.step > 1 ? "#ff4d74" : ""}`
            }}
          />
        </div>
        <div
          className="step"
          style={{
            backgroundColor: `${this.props.step > 2 ? "#ff4d74" : ""}`
          }}
        >
          <i
            className="fas fa-play"
            style={{
              color: `${this.props.step > 2 ? "#ff4d74" : ""}`
            }}
          />
        </div>
        <div
          className="step"
          style={{
            backgroundColor: `${this.props.step > 3 ? "#ff4d74" : ""}`
          }}
        >
          <i
            className="fas fa-play"
            style={{
              color: `${this.props.step > 3 ? "#ff4d74" : ""}`
            }}
          />
        </div>
        <div
          className="step"
          style={{
            backgroundColor: `${this.props.step > 4 ? "#ff4d74" : ""}`
          }}
        >
          <i
            className="fas fa-play"
            style={{
              color: `${this.props.step > 4 ? "#ff4d74" : ""}`
            }}
          />
        </div>
      </div>
    );
  }
}

export default Stepper;
