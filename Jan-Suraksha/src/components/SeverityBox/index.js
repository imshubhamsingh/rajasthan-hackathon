import React, { Component } from "react";

import "./severitybox.scss";

class SeverityBox extends Component {
  state = {
    high: false,
    medium: false,
    slight: false,
    normal: false
  };

  select = value => {
    this.setState({
      high: false,
      medium: false,
      slight: false,
      normal: false
    });
    if (value === "high") {
      this.setState({ high: true });
    } else if (value === "medium") {
      this.setState({ medium: true });
    } else if (value === "slight") {
      this.setState({ slight: true });
    } else {
      this.setState({ normal: true });
    }
    this.props.sendValue(value);
  };

  render() {
    return (
      <div className="box">
        <div
          className={`option " + ${this.state.high ? "selected" : ""}`}
          onClick={() => {
            this.select("high");
          }}
        >
          High
        </div>
        <div
          className={`option " + ${this.state.medium ? "selected" : ""}`}
          onClick={() => {
            this.select("medium");
          }}
        >
          Medium
        </div>
        <div
          className={`option " + ${this.state.slight ? "selected" : ""}`}
          onClick={() => {
            this.select("slight");
          }}
        >
          Slight
        </div>
        <div
          className={`option " + ${this.state.normal ? "selected" : ""}`}
          onClick={() => {
            this.select("normal");
          }}
        >
          Normal
        </div>
      </div>
    );
  }
}

export default SeverityBox;
