import React, { Component } from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";

import "./map.scss";

class MapContainer extends Component {
  static defaultProps = {
    center: { lat: 53.27, lng: -7.77 },
    zoom: 11
  };

  componentDidMount() {
    this.getCurrentCoordinates();
  }

  getCurrentCoordinates = () => {
    console.log("in getCurrentCoordinates", new Date());
    var options = {
      enableHighAccuracy: false,
      timeout: 5000,
      maximumAge: 0
    };
    navigator.geolocation.getCurrentPosition(
      pos => {
        console.log(pos);
        console.log("from react!");
      },
      err => {
        // console.log("error occured", err);
      },
      options
    );
  };

  render() {
    return (
      <div className="google-map">
        <Map
          google={this.props.google}
          zoom={14}
          initialCenter={{
            lat: 40.854885,
            lng: -88.081807
          }}
          style={{
            borderRadius: "20px",
            width: "96vw",
            margin: "0 auto",
            height: "300px"
          }}
          zoom={15}
        >
          <Marker onClick={this.onMarkerClick} name={"Current location"} />
        </Map>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyDi5PICtJHnO_Od57WSiJEMgOsFSTXpVUA"
})(MapContainer);
