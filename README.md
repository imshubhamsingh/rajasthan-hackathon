# Jan Suraksha


Nearly 1.3 million people die every year on the world's roads, and up to 50 million are injured.
Traffic fatalities measured by the proxy of motor vehicle registrations and population, increasing
traffic volume leads to an increase in fatalities per capita.


![JanSuraksha](./screenshots/mainScreen.png)
![JanSuraksha](./screenshots/2.png)
![JanSuraksha](./screenshots/3.png)
JanSuraksha is a android/iOS and webapp that would
consolidate and bring out a new easy way for citizens to report
road accidents. With JanSuraksha, every citizen gets an account
that is e-linked to their Aadhaar card. Whenever they see a road
accident victim on the road, they can open the app and report
the details of the accident. Emergency first responders would
have a dedicated account for them and any incident happening
in their area would send an emergency alert to them via the app.
This alert will have all the information about what the incident is,
when it happened, how severe is it.
![JanSuraksha](./screenshots/4.png)
![JanSuraksha](./screenshots/5.png)
![JanSuraksha](./screenshots/6.png)
![JanSuraksha](./screenshots/7.png)
![JanSuraksha](./screenshots/8.png)

Road accident is
becoming more and more common in
today’s society and contributes to a
significant number of deaths as the
result. The number of road accidents
recorded in the state of Rajasthan in
2011 was 23,245. The data complied by
the National Crime Records
Bureau (NCRB) claims that the number
accounts for 5.3% of the total cases of
road accidents registered in the
country.

## Potentially huge impact

In a report in 2006, the Law Commission
estimated that 50 per cent of accident victims would have survived
had they got medical attention within an hour.
